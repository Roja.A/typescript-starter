const zip = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<[number, number]> => {
  const result = [];
  for (let i = 0; i < arr1.length; i++) {
    result.push([arr1[i], arr2[i]]);
  }
  return result;
};
export default zip;
