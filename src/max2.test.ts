import max from './max2';

test('max', () => {
  expect(max(2, 3)).toEqual(3);
  expect(max(20, 2)).toEqual(20);
  expect(max(5, 6)).toEqual(6);
  expect(max(10, 20)).toEqual(20);
  expect(max(1, 2)).toEqual(2);
});
