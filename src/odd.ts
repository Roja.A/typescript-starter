const isOdd = (n: number): boolean => {
  return n % 2 !== 0 ? true : false;
};
const isEven = (n: number): boolean => {
  return n % 2 === 0 ? true : false;
};
export { isOdd, isEven };
