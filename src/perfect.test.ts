import perfect from './perfect';
test('perfect', () => {
  expect(perfect(6)).toEqual(true);
  expect(perfect(8)).toEqual(false);
  expect(perfect(4)).toEqual(false);
  expect(perfect(28)).toEqual(true);
});
