const range = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i < stop; ++i) {
    result.push(i);
  }
  return result;
};

const factors = (n: number): number[] => range(1, n).filter(i => n % i === 0);

const isPrime = (n: number): boolean => factors(n).length === 1;

const primeNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop + 1).filter(isPrime);
export default primeNumbers;
