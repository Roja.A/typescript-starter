import range from './primenumbers';
const pascalLine = (n: number): number[] => range(0, n + 1).map(r => ncr(n, r));

const pascalTrangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range1(0, lines + 1).map(lines => pascalLine(lines));
console.log(pascalTrangle(3));
