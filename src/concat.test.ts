import concate from './concate';
test('concate', () => {
  expect(concate([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concate([1, 4, 3], [6, 7, 8])).toEqual([1, 4, 3, 6, 7, 8]);
  expect(concate([1, 5], [9, 10])).toEqual([1, 5, 9, 10]);
  expect(concate([1, 2, 3], [10, 20, 30])).toEqual([1, 2, 3, 10, 20, 30]);
});
