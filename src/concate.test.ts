import concat from './concate';
test('concat', () => {
  expect(concat([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concat([2, 4, 5], [6, 7, 8])).toEqual([2, 4, 5, 6, 7, 8]);
  expect(concat([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
});
