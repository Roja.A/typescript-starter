const takeWhile = (
  arr: ReadonlyArray<number>,
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const ar = [];
  for (const e of arr) {
    if (f(e)) {
      ar.push(e);
    } else {
      break;
    }
  }
  return ar;
};
export default takeWhile;
