import maxValue from './maxWithReduce';
test('maxValue', () => {
  expect(maxValue([1, 2, 3, 4, 5])).toEqual(5);
  expect(maxValue([1, 2, 10, 4, 5])).toEqual(10);
  expect(maxValue([1, 4, 3, 7, 5])).toEqual(7);
  expect(maxValue([1, 2, 9, 4, 15])).toEqual(15);
  expect(maxValue([1, 2, 3, 23])).toEqual(23);
});
