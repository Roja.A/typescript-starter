const isFactor = (n: number, i: number): number => {
  if (i % n === 0) {
    return i;
  }
};
export default isFactor;
