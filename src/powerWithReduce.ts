const powerValue = (base: number, power: number): number => {
  let ar = [];
  for (let i = 0; i < power; i++) {
    ar.push(base);
  }
  return ar.reduce((mul, v) => mul * v);
};
export default powerValue;
