const Repeat = (x: number, n: number): ReadonlyArray<number> => {
  const zeroFillArr = [];
  for (let z = 0; z < n; z++) {
    zeroFillArr.push(0);
  }
  return zeroFillArr.map(() => x);
};
export default Repeat;
