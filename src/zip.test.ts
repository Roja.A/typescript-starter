import zip from './zip';
test('zip', () => {
  expect(zip([1, 2, 3], [4, 5, 6])).toEqual([[1, 4], [2, 5], [3, 6]]);
  expect(zip([1, 2], [5, 6])).toEqual([[1, 5], [2, 6]]);
  expect(zip([1], [4])).toEqual([[1, 4]]);
  expect(zip([1, 2, 3, 4], [7, 8, 9, 10])).toEqual([
    [1, 7],
    [2, 8],
    [3, 9],
    [4, 10]
  ]);
});
