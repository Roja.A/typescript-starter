import primeNumbers from './primenumbers';
test('primeNumbers', () => {
  expect(primeNumbers(1, 5)).toEqual([2, 3, 5]);
  expect(primeNumbers(1, 10)).toEqual([2, 3, 5, 7]);
  expect(primeNumbers(1, 15)).toEqual([2, 3, 5, 7, 11, 13]);
  expect(primeNumbers(1, 20)).toEqual([2, 3, 5, 7, 11, 13, 17, 19]);
});
