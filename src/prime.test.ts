import prime from './prime';
test('prime', () => {
  expect(prime(3)).toEqual(true);
  expect(prime(4)).toEqual(false);
  expect(prime(5)).toEqual(true);
});
