const max = (arr: number[]): number => {
  let max1: number[] = arr[0];
  for (const i of arr) {
    if (arr[i] >= max1) {
      max1 = arr[i];
    }
  }
  return max1;
};
export default max;
