const gcd = (x: number, y: number): number => {
  if (!y) {
    return x;
  }
  return gcd(y, x % y);
};
class Rational {
  numarator: number;
  denominator: number;
  constructor(numarator: number, denominator: number) {
    const g = gcd(numarator, denominator);
    this.numarator = numarator / g;
    this.denominator = denominator / g;
  }
  add(t: Rational): Rational {
    const numarator =
      this.numarator * t.denominator + this.denominator * t.numarator;
    const denominator = this.denominator * t.denominator;
    const res = new Rational(numarator, denominator);
    return res;
  }
  subtract(t: Rational): Rational {
    const numarator =
      this.numarator * t.denominator - this.denominator * t.numarator;
    const denominator = this.denominator * t.denominator;
    const res = new Rational(numarator, denominator);
    return res;
  }
  multiple(t: Rational): Rational {
    const numarator =
      this.numarator * t.denominator * this.denominator * t.numarator;
    const denominator = this.denominator * t.denominator;
    const res = new Rational(numarator, denominator);
    return res;
  }
  divide(t: Rational): Rational {
    const numarator =
      this.numarator / t.denominator * this.denominator / t.numarator;
    const denominator = this.denominator * t.denominator;
    const res = new Rational(numarator, denominator);
    return res;
  }
  compare(t1: Rational): number {
    return this.numarator * t1.denominator - this.denominator * t1.numarator;
  }

  less = (t1: Rational): boolean => {
    return this.compare(t1) < 0;
  };
  equal = (t1: Rational): boolean => {
    return this.compare(t1) === 0;
  };
  greter = (t1: Rational): boolean => {
    return this.compare(t1) > 0;
  };
}
const r1 = new Rational(6, 2);
const r2 = new Rational(4, 2);
const r3 = r1.add(r2);
const r4 = r1.subtract(r2);
const r5 = r1.multiple(r2);
const r6 = r1.divide(r2);
const r7 = r1.compare(r2);
console.log(r3.numarator, r3.denominator);
console.log(r4.numarator, r4.denominator);
console.log(r5.numarator, r5.denominator);
console.log(r6.numarator, r6.denominator);
console.log(r7);
