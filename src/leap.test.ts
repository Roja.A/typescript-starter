import isLeap from './leap';
test('isLeap', () => {
  expect(isLeap(2016)).toEqual(true);
  expect(isLeap(2000)).toEqual(true);
  expect(isLeap(2017)).toEqual(false);
});
