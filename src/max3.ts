const max3 = (a: number, b: number, c: number): number => {
  return a > b ? a : b > c ? b : c;
};
export default max3;
