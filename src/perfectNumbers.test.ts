import perfectNumber from './perfectnumbers';
test('perfectNumber', () => {
  expect(perfectNumber(1, 10)).toEqual([1, 6]);
  expect(perfectNumber(1, 30)).toEqual([1, 6, 28]);
  expect(perfectNumber(1, 20)).toEqual([1, 6]);
  expect(perfectNumber(1, 7)).toEqual([1, 6]);
});
