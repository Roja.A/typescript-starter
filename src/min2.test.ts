import min from './min2';
test('min', () => {
  expect(min(2, 4)).toEqual(2);
  expect(min(5, 4)).toEqual(4);
  expect(min(6, 8)).toEqual(6);
  expect(min(9, 10)).toEqual(9);
});
