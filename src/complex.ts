class Complex {
  real: number;
  imaginary: number;
  constructor(real: number, imaginary: number) {
    this.real = real;
    this.imaginary = imaginary;
  }
  add(a: Complex): string {
    const real = this.real + a.real;
    const img = this.imaginary + a.imaginary;
    return '' + real + '+' + img + 'i';
  }
  subtract(a: Complex): string {
    const real = this.real - a.real;
    const img = this.imaginary - a.imaginary;
    return '' + real + '-' + img + 'i';
  }
  multiple(a: Complex): string {
    const real = this.real * a.real - this.imaginary * a.imaginary;
    const img = this.real * a.imaginary + this.imaginary * a.real;
    return '' + real + '+' + img + 'i';
  }

  div(a: Complex): string {
    const real = this.real / a.real;
    const img = this.imaginary / a.imaginary;
    return '' + real + '+' + img + 'i';
  }
  compare(a1: Complex): number {
    let retVal;
    if (this.real > a1.real) {
      retVal = 1;
    }
    if (this.real < a1.real) {
      retVal = -1;
    }
    if (this.real === a1.real) {
      retVal = 0;
    }
    return retVal;
  }
}

const c1 = new Complex(2, 3);
const c2 = new Complex(4, 2);
const c3 = c1.add(c2);
const c4 = c1.subtract(c2);
const c5 = c1.multiple(c2);
const c6 = c1.div(c2);
const c7 = c1.compare(c2);
console.log(c3);
console.log(c4);
console.log(c5);
console.log(c6);
console.log(c7);
