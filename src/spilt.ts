const splitAt = (
  arr: ReadonlyArray<number>,
  n: number
): [ReadonlyArray<number>, ReadonlyArray<number>] => {
  const retArr = [];
  let index = 0;
  for (let i = 0; i < n; i++) {
    const locArr = [];
    for (let j = 0; j < arr.length / n; j++) {
      locArr.push(arr[index]);
      index++;
    }
    retArr.push(locArr);
  }
  return retArr;
};

export default splitAt;
