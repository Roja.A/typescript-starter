import max3 from './max3';
test('max3', () => {
  expect(max3(1, 2, 3)).toEqual(3);
});
