const push = (arr: ReadonlyArray<number>, e: number) => [...arr, e];
const concate = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (let i = 0; i < arr2.length; i++) {
    arr1 = push(arr1, arr2[i]);
  }

  return arr1;
};
export default concate;
