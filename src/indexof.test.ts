import indexOf from './indexof';
test('indexOf', () => {
  expect(indexOf([2, 3, 4, 5], 3)).toEqual(1);
  expect(indexOf([2, 3, 4, 5], 6)).toEqual(-1);
  expect(indexOf([2, 3, 4, 5], 5)).toEqual(3);
  expect(indexOf([2, 3, 4, 5], 4)).toEqual(2);
});
