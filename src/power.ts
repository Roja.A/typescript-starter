const power = (x: number, y: number): number => {
  let result: number = 1;
  if (y === 0) {
    return 1;
  }
  while (y !== 0) {
    result = result * x;
    --y;
  }
  return result;
};
export default power;
