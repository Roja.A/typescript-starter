const max = (x: number, y: number): number => {
  return x > y ? x : y;
};
export default max;
