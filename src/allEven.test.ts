import allEven from './allEven';
test('allEven', () => {
  expect(allEven([1, 2, 3, 4, 5, 6])).toEqual([2, 4, 6]);
  expect(allEven([1, 2, 3, 7, 5, 6])).toEqual([2, 6]);
  expect(allEven([1, 2, 3, 4, 5, 9, 11])).toEqual([2, 4]);
  expect(allEven([1, 2, 3, 5, 7, 9])).toEqual([2]);
});
