import dropWhile from './dropWhile';
test('dropWhile', () => {
  expect(dropWhile([2, 3, -4], x => x > 0)).toEqual([-4]);
  expect(dropWhile([-5, -6, 7], x => x > 0)).toEqual([]);
});
