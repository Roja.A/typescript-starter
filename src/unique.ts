const unique = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  const ar = [];
  let cnt = 0;
  for (let i = 0; i < arr.length; i++) {
    cnt++;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        cnt++;
      }
    }
    if (cnt === 1) {
      ar.push(arr[i]);
    }
    cnt = 0;
  }
  return ar;
};
export default unique;
