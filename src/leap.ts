const isLeap = (year: number): boolean => {
  return year % 4 === 0 ? true : false;
};
export default isLeap;
