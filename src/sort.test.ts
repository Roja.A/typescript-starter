import sort from './sort';
test('sort', () => {
  expect(sort([1, 3, 2, 6])).toEqual([1, 2, 3, 6]);
  expect(sort([1, 10, 4, 78, 8])).toEqual([1, 4, 8, 10, 78]);
  expect(sort([5, 3, 8, 2, 9, 1])).toEqual([1, 2, 3, 5, 8, 9]);
  expect(sort([5, 90, 21, 56, 2])).toEqual([2, 5, 21, 56, 90]);
});
