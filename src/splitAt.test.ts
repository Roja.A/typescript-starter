import splitAt from './spilt';
test('splitAt', () => {
  expect(splitAt([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
  expect(splitAt([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
  expect(splitAt([1, 6, 7, 8, 9, 10], 3)).toEqual([[1, 6], [7, 8], [9, 10]]);
  expect(splitAt([1, 2, 3, 4, 5, 6], 2)).toEqual([[1, 2, 3], [4, 5, 6]]);
});
