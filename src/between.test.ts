import liesBetween from './between';
test('liesBetween',()=>{
  expect(liesBetween(2,4,3)).toEqual(true);
  expect(liesBetween(2,5,1)).toEqual(false);
  expect(liesBetween(2,4,3)).toEqual(true);
};