import isFactor from './isFactor';
test('isFactor', () => {
  expect(isFactor(2, 2)).toEqual(2);
  expect(isFactor(2, 4)).toEqual(4);
  expect(isFactor(2, 6)).toEqual(6);
});
