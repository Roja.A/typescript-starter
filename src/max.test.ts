import max from './max';
test('max', () => {
  expect(max([1, 2, 3, 4])).toEqual(4);
  expect(max([1, 55, 10, 20])).toEqual(55);
  expect(max([1, 16, 3])).toEqual(16);
  expect(max([2, 7, 8])).toEqual(8);
  expect(max([3, 8, 1, 5])).toEqual(8);
});
