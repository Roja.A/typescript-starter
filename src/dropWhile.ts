const dropWhile = (
  arr: ReadonlyArray<number>,
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const res = [];
  for (const i of arr) {
    if (f(i)) {
      res.push(arr[i]);
    }
    return res;
  }
};
export default dropWhile;
