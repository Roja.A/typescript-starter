const range = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i < stop; ++i) {
    result.push(i);
  }
  return result;
};

const factors = (n: number): number[] => range(1, n).filter(i => n % i === 0);

const add1 = (x: number, y: number): number => x + y;
const Perfect = (n: number): boolean =>
  n === 1 ? true : factors(n).reduce(add1) === n;

const perfectNumber = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(Perfect);

export default perfectNumber;
