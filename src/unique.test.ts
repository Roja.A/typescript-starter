import unique from './unique';
test('unique', () => {
  expect(unique([1, 1, 2, 2, 3, 3])).toEqual([1, 2, 3]);
  expect(unique([1, 1, 2, 2, 4])).toEqual([1, 2, 4]);
  expect(unique([1, 1, 5, 6])).toEqual([1, 5, 6]);
  expect(unique([3, 3, 4, 4, 5, 5, 8])).toEqual([3, 4, 5, 8]);
});
