const allEven = (arr: number[]): number[] => {
  const res: number[] = [];
  for (let i of arr) {
    if (arr[i] % 2 === 0) {
      res.push(arr[i]);
    }
  }
  return res;
};
export default allEven;
