const prime = (n: number): boolean => {
  for (let i = 2; i <= n; i++) {
    return n % i === 0 ? false : true;
  }
};
export default prime;
