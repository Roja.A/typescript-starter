import powerValue from './powerWithReduce';
test('powerValue', () => {
  expect(powerValue(2, 3)).toEqual(8);
  expect(powerValue(2, 2)).toEqual(4);
  expect(powerValue(4, 2)).toEqual(16);
  expect(powerValue(5, 2)).toEqual(25);
});
