import { isOdd, isEven } from './odd';
test('isOdd', () => {
  expect(isOdd(2)).toEqual(false);
  expect(isOdd(3)).toEqual(true);
  expect(isOdd(4)).toEqual(false);
  expect(isOdd(5)).toEqual(true);
});
test('isEven', () => {
  expect(isEven(2)).toEqual(true);
  expect(isEven(3)).toEqual(false);
  expect(isEven(4)).toEqual(true);
  expect(isEven(5)).toEqual(false);
});
