import takeWhile from './takeWhile';
test('takeWhile', () => {
  expect(takeWhile([2, 3, 4], x => x % 2 === 0)).toEqual([2]);
  expect(takeWhile([3, 4], x => x % 2 === 0)).toEqual([]);
  expect(takeWhile([4, 5, 6], x => x % 2 === 0)).toEqual([4]);
  expect(takeWhile([1, 2, 3], x => x % 2 === 0)).toEqual([]);
});
