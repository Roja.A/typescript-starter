import squareAll from './squareAll';
test('squareAll', () => {
  expect(squareAll([1, 2, 3, 4])).toEqual([1, 4, 9, 16]);
  expect(squareAll([1, 1, 3, 5])).toEqual([1, 1, 9, 25]);
  expect(squareAll([1, 6, 7, 8])).toEqual([1, 36, 49, 64]);
  expect(squareAll([1, 2, 4])).toEqual([1, 4, 16]);
});
