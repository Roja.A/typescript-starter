const fact = (n: number): number => {
  if (n === 0) {
    return 1;
  }
  return n * fact(n - 1);
};
const ncr = (n: number, r: number): number => {
  return n < r ? 0 : fact(n) / (fact(r) * fact(n - r));
};

export { fact, ncr };
