import power from './power';
test('power', () => {
  expect(power(2, 2)).toEqual(4);
  expect(power(2, 3)).toEqual(8);
  expect(power(3, 2)).toEqual(9);
  expect(power(4, 2)).toEqual(16);
});
