const perfect = (n: number): boolean => {
  const t: number = 0;
  for (let i = 1; i < n; i++) {
    if (n % i === 0) {
      t = t + i;
    }
  }
  return t === n ? true : false;
};
export default perfect;
