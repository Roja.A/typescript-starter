import { fact, ncr } from './factorial';
test('fact', () => {
  expect(fact(4)).toEqual(24);
  expect(fact(2)).toEqual(2);
  expect(fact(1)).toEqual(1);
  expect(fact(0)).toEqual(1);
});
test('ncr', () => {
  expect(ncr(1, 1)).toEqual(1);
  expect(ncr(2, 2)).toEqual(1);
  expect(ncr(3, 3)).toEqual(1);
  expect(ncr(4, 4)).toEqual(1);
});
