import Repeat from './repeat';
test('Repeat', () => {
  expect(Repeat(2, 3)).toEqual([2, 2, 2]);
  expect(Repeat(5, 2)).toEqual([2, 2]);
  expect(Repeat(6, 1)).toEqual([1]);
  expect(Repeat(4, 5)).toEqual([4, 4, 4, 4, 4]);
});
